# A class to use the accounts module to create all users with their expected parameters.
#
# @summary Creates the users via the accounts module
#
class account_management::users {
  $required_users = $account_management::required_users
  $required_users.each |$username, $params| {

    # Check if the keyline is valid
    if 'sshkeys' in $params {
      $params['sshkeys'].each |$idx, $key| {
        $key_is_valid = accounts_validate_authorized_key_line($key)
        unless($key_is_valid) {
          $message = @("EOT"/L)
            user: ${username} sshkey at index ${idx} is invalid!
            |-EOT
          fail($message)
        }
      }
    }

    # Disable group creation if the group isn't the username
    if 'group' in $params {
      $primary_group = $params['group']
      if $username == $primary_group {
        $group_params = {
          'create_group' => true,
        }
      } else {
        $group_params = {
          'create_group' => false,
        }
      }
    } else {
      $group_params = {
        'create_group' => true,
      }
    }
    $arg_params = $params + $group_params
    accounts::user { $username:
      * => $arg_params,
    }
  }

  unless(empty($account_management::exec_map)) {
    $account_management::exec_map.each |$username, $exec_list| {
      unless($username in $account_management::required_users) {
        fail("Exec list cannot be processed for undefined user '${username}'!")
      }

      # Create an exec resource for each entry in the attached list
      $exec_list.each |$index, $exec| {
        unless('refreshonly' in $exec or 'creates' in $exec or 'onlyif' in $exec) {
          $error_mesasge = @("EOT"/L)
            Exec ${username} ${index}: at least one of 'creates', 'onlyif', or \
            'refreshonly' must be specified!
            |-EOT
          fail($error_mesasge)
        }

        exec { "${username}_${index}":
          user      => $username,
          subscribe => Accounts::User[$username],
          *         => $exec,
        }
      }
    }
  }

  unless(empty($account_management::cron_map)) {
    $account_management::cron_map.each |$username, $cron_list| {
      unless($username in $account_management::required_users) {
        fail("Cron list cannot be processed for undefined user '${username}'!")
      }

      # Create an cron resource for each entry in the attached list
      $cron_list.each |$index, $cron| {
        cron { "${username}_${index}":
          user    => $username,
          require => Accounts::User[$username],
          *       => $cron,
        }
      }
    }
  }
}
