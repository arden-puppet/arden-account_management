# A class to handle group creation
#
# @summary Creates groups.
#
class account_management::groups {
  $required_groups = $account_management::required_groups

  # Creates the groups with the required parameters
  $required_groups.each |$group_name, $group_params| {
    group { $group_name:
      ensure => 'present',
      *      => $group_params,
    }
  }
}
