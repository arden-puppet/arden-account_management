# Parse an ssh authorized_keys line string and return a boolean message
# indicating whether this line is valid or not.
Puppet::Functions.create_function(:accounts_validate_authorized_key_line) do
  # @param str ssh authorized_keys line string
  # @return [Boolean] indicating whether the line is valid or not
  # @example Calling the function
  #   accounts_validate_authorized_key_line_string('options ssh-rsa AAAA... comment)
  dispatch :accounts_validate_authorized_key_line_string do
    param 'String', :str
  end

  def accounts_validate_authorized_key_line_string(str)
    matched = str.match(%r{((ssh-|ecdsa-)[^\s]+)\s+([^\s]+)\s+(.*)$})
    if matched && matched.length == 5
      true
    else
      false
    end
  end
end
