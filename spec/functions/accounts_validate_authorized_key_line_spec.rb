require 'spec_helper'

describe 'accounts_validate_authorized_key_line' do
  it 'is defined' do
    is_expected.not_to eq(nil)
  end
  it 'rejects blank lines' do
    is_expected.to run.with_params('').and_return(false)
  end
  it 'rejects unknown keytypes' do
    is_expected.to run.with_params('options unknown-keytype key comment').and_return(false)
  end
  it 'accepts ssh types' do
    is_expected.to run.with_params('options ssh-keytype key comment').and_return(true)
  end
  it 'accepts ecdsa types' do
    is_expected.to run.with_params('options ecdsa-keytype key comment').and_return(true)
  end
  it 'rejects valid lines which are missing parameters' do
    is_expected.to run.with_params('ssh-keytype key').and_return(false)
  end
end
