require 'spec_helper'
require 'deep_merge'

shell_map = {
  'Debian' => {
    'zsh' => '/usr/bin/zsh',
  },
  'Gentoo' => {
    'zsh' => '/bin/zsh',
  },
  'RedHat' => {
    'zsh' => '/bin/zsh',
  },
}

describe 'account_management' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:zsh_path) { shell_map[os_facts[:osfamily]]['zsh'] }
      let(:sshkey_good) { 'ssh-rsa IamKeyData rstallman@host' }
      let(:sshkey_bad) { 'somegarbage-rsa IwishIwasKeyData' }
      let(:params) do
        {
          'required_users' => {
            'rstallman' => {
              'comment' => 'Gnu/Linux',
              'groups' => ['fsf', 'emacs'],
              'uid' => '1500',
              'gid' => '1500',
              'shell' => zsh_path,
              'password' => 'hunter2',
              'sshkeys' => [
                sshkey_good,
              ],
            },
          },
          'required_groups' => {
            'fsf' => {
              'name' => 'fsf',
              'gid'  => '2000',
            },
            'emacs' => {
              'name' => 'emacs',
              'gid' => '2001',
            },
          },
        }
      end

      context 'vaild config' do
        it { is_expected.to compile.with_all_deps }

        # Make sure the subclasses are there with the proper relationships
        it 'contains the subclasses with the correct relationships' do
          is_expected.to contain_class('account_management::groups')
          is_expected.to contain_class('account_management::shells').that_requires('Class[account_management::groups]')
          is_expected.to contain_class('account_management::users').that_requires('Class[account_management::shells]')
        end

        it 'creates the correct users and primary groups' do
          is_expected.to contain_accounts__user('rstallman').with(
            ensure: 'present',
            comment: 'Gnu/Linux',
            uid: '1500',
            groups: ['fsf', 'emacs'],
            shell: zsh_path,
          )

          is_expected.to contain_group('rstallman').with(
            ensure: 'present',
            gid: '1500',
          )
        end

        it 'creates each additional group' do
          is_expected.to contain_group('fsf').with(
            'ensure' => 'present',
            'gid'    => '2000',
          )
          is_expected.to contain_group('emacs').with(
            'ensure' => 'present',
            'gid'    => '2001',
          )
        end

        it 'disables group creation when a non-user primary group is specified' do
          params['required_users']['rstallman']['group'] = 'weirdo'
          is_expected.to contain_accounts__user('rstallman').with(
            ensure: 'present',
            comment: 'Gnu/Linux',
            uid: '1500',
            groups: ['fsf', 'emacs'],
            shell: zsh_path,
            group: 'weirdo',
            create_group: false,
          )
        end

        context 'deploys the correct shells' do
          case os_facts[:osfamily]
          when 'Gentoo'
            it 'installs the Gentoo package' do
              is_expected.to contain_package('app-shells/zsh').with(
                ensure: 'present',
              )
            end
          when 'RedHat'
            it 'installs the RedHat package' do
              is_expected.to contain_package('zsh').with(
                ensure: 'present',
              )
            end
          end
        end

        context 'with exec and cron lists' do
          before(:each) do
            params.merge!(
              exec_map: {
                'rstallman' => [
                  {
                    'command' => '/usr/bin/git clone git://git.sv.gnu.org/hurd/hurd.git ',
                    'creates' => '/home/rstallman/hurd',
                  },
                  {
                    'command' => '/usr/bin/git clone https://gcc.gnu.org/git/gcc.git',
                    'creates' => '/home/rstallman/gcc',
                  },
                ],
              },
              cron_map: {
                'rstallman' => [
                  {
                    'command' => 'cd /home/rstallman/hurd || exit 1; /usr/bin/git pull --all',
                    'hour' => '0',
                    'weekday' => '0',
                  },
                  {
                    'command' => 'cd /home/rstallman/gcc || exit 1; /usr/bin/git pull --all',
                    'hour' => '0',
                    'weekday' => '1',
                  },
                ],
              },
            )
          end

          it 'compiles' do
            is_expected.to compile.with_all_deps
          end

          it 'runs the commands' do
            params[:exec_map].each do |user, commands|
              commands.each_index do |index|
                is_expected.to contain_exec("#{user}_#{index}").with(
                  user: user,
                  command: commands[index]['command'],
                  creates: commands[index]['creates'],
                ).that_subscribes_to("Accounts::User[#{user}]")
              end
            end
          end

          it 'schedules the cron tasks' do
            params[:cron_map].each do |user, crons|
              crons.each_index do |index|
                is_expected.to contain_cron("#{user}_#{index}").with(
                  user: user,
                  command: crons[index]['command'],
                  hour: crons[index]['hour'],
                  weekday: crons[index]['weekday'],
                ).that_requires("Accounts::User[#{user}]")
              end
            end
          end
        end
      end

      context 'un-mapped shell' do
        let(:params) do
          {
            'required_users' => {
              'aixman' => {
                'comment' => 'Path Sabotage',
                'groups' => ['system'],
                'uid' => '1500',
                'gid' => '1500',
                'shell' => '/usr/bin/csh',
                'password' => 'hunter7',
              },
            },
            'required_groups' => {
              'system' => {
                'gid'  => '0',
              },
            },
          }
        end

        it 'fails to compile' do
          is_expected.to compile.and_raise_error(%r{Un-mapped shell '/usr/bin/csh' specified for user 'aixman'!})
        end
      end

      context 'cron entry for missing user' do
        before(:each) do
          params.merge!(
            cron_map: {
              'martha-w' => [
                {
                  'command' => '/usr/bin/locate',
                  'hour' => '10',
                },
              ],
            },
          )
        end

        it 'compiles and raises error' do
          is_expected.to compile.and_raise_error(%r{Cron list cannot be processed for undefined user 'martha-w'!})
        end
      end

      context 'exec entry for missing user' do
        before(:each) do
          params.merge!(
            exec_map: {
              'martha-w' => [
                {
                  'command' => 'yum install locate',
                },
              ],
            },
          )
        end

        it 'compiles and raises error' do
          is_expected.to compile.and_raise_error(%r{Exec list cannot be processed for undefined user 'martha-w'!})
        end
      end

      context 'user has an invalid sshkey' do
        before(:each) do
          params.deep_merge!(
            'required_users' => {
              'rstallman' => {
                'sshkeys' => [
                  sshkey_bad,
                ],
              },
            },
          )
        end

        it 'compiles and raises error' do
          is_expected.to compile.and_raise_error(%r{user: rstallman sshkey at index 1 is invalid!})
        end
      end

      context 'exec entry missing refreshonly/creates/onlyif' do
        before(:each) do
          params.merge!(
            exec_map: {
              'rstallman' => [
                {
                  'command' => '/usr/bin/git clone git://git.sv.gnu.org/hurd/hurd.git ',
                },
              ],
            },
          )
        end

        it 'compiles and raises error' do
          is_expected.to compile.and_raise_error(%r{Exec rstallman 0: at least one of 'creates', 'onlyif', or 'refreshonly' must be specified!})
        end

        it 'compiles with onlyif' do
          params[:exec_map]['rstallman'][0]['onlyif'] = '/bin/test 1 -eq 1'
          is_expected.to compile.with_all_deps
        end

        it 'compiles with refreshonly' do
          params[:exec_map]['rstallman'][0]['refreshonly'] = 'true'
          is_expected.to compile.with_all_deps
        end

        it 'compiles with creates' do
          params[:exec_map]['rstallman'][0]['creates'] = '/home/rstallman/hurd'
          is_expected.to compile.with_all_deps
        end
      end
    end
  end
end
