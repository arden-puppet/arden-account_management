require 'spec_helper_acceptance'

describe 'account_management' do
  context 'standard config with one user' do
    let(:rsa_key1) do
      <<~HEREDOC
        ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDL4bJzFWxIp4qRZJjtyfqallN86zo9UQE
        4WjBaSb8esVsQY2dnPk6Vq5jsbaBnHKy3f7Ygf6zRhyJvbfO37dwgtVxE/kAVaxBRhnjiCZ
        5WcV/KNKLOh+OG/JTtTkKkR6Z0+mxkGuCP0E7wBgk5aZKvwXCTO0q9ciGZL4sYXp4DG1liI
        C8OWxGsHpMhG90BBORspJw2feGBAXLi48e4vrZQLF5gvI8ubNEEc+XFDg31N4BPzLCfwowj
        HClt8M2o4oXFOA1dJT3zIFURDZckP7YwpNfi0UPXvY80HlzeWNxW7nJ8iYkDL/3pcfRfeM4
        rcmaApDffNrtiBvS486wRxU8wnQxGbvI/AIEGLtFnh/iNATliskzuzF3jOXosKMRQja8W2U
        HD0KjihrLRMZO+U1zZMs0RatOOTbN3453IQiV9ifNNChtVAxZ7lVCEUMeb/MgK9uUPZDuI4
        NuZsoMbiD7wX21JL2ONCoXAXleg/gYAA+bnhCT9MRDE2WpngAsUoFk= wriker@titan
      HEREDOC
    end
    let(:rsa_key2) do
      <<~HEREDOC
        ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDL4bJzFWxIp4qRZJjtyfqallN86zo9UQE
        4WjBaSb8esVsQY2dnPk6Vq5jsbaBnHKy3f7Ygf6zRhyJvbfO37dwgtVxE/kAVaxBRhnjiCZ
        5WcV/KNKLOh+OG/JTtTkKkR6Z0+mxkGuCP0E7wBgk5aZKvwXCTO0q9ciGZL4sYXp4DG1liI
        C8OWxGsHpMhG90BBORspJw2feGBAXLi48e4vrZQLF5gvI8ubNEEc+XFDg31N4BPzLCfwowj
        HClt8M2o4oXFOA1dJT3zIFURDZckP7YwpNfi0UPXvY80HlzeWNxW7nJ8iYkDL/3pcfRfeM4
        rcmaApDffNrtiBvS486wRxU8wnQxGbvI/AIEGLtFnh/iNATliskzuzF3jOXosKMRQja8W2U
        HD0KjihrLRMZO+U1zZMs0RatOOTbN3453IQiV9ifupdating-some-stuff-here-like-a
        -bossMbiD7wX21JL2ONCoXAXleg/gYAA+bnhCT9MRDE2WpngAsUoFk= wriker@defiant
      HEREDOC
    end
    let(:manifest1) do
      <<-EOS
        class { 'account_management':
          required_groups => {
            'bridge' => {
              'name' => 'bridge',
              'gid'  => '1701',
            },
          },
          required_users => {
            'wriker' => {
              'comment' => 'beard',
              'groups'  => [
                'wheel',
                'bridge',
              ],
              'uid'     => '1200',
              'gid'     => '1200',
              'shell'   => '/bin/zsh',
              'sshkeys' => [
                '#{rsa_key1}',
              ],
            },
          },
          exec_map       => {
            'wriker' => [
              {
                'command' => 'touch ./baz',
                'cwd'     => '/home/wriker',
                'path'    => '/usr/local/bin:/usr/bin',
                'creates' => '/home/wriker/baz',
              },
            ],
          },
        }
      EOS
    end
    let(:manifest2) do
      <<-EOS
        class { 'account_management':
          required_groups => {
            'bridge' => {
              'name' => 'bridge',
              'gid'  => '1701',
            },
          },
          required_users => {
            'wriker' => {
              'comment' => 'Secretly Thomas',
              'groups'  => [
                'wheel',
                'bridge',
              ],
              'uid'     => '1200',
              'gid'     => '1200',
              'shell'   => '/bin/zsh',
              'sshkeys' => [
                '#{rsa_key2}',
              ],
            },
          },
          exec_map       => {
            'wriker' => [
              {
                'command' => 'touch ./baz',
                'cwd'     => '/home/wriker',
                'path'    => '/usr/local/bin:/usr/bin',
                'creates' => '/home/wriker/baz',
              },
            ],
          },
        }
      EOS
    end

    it 'applies without errors and is idempotent on the second run' do
      apply_manifest(manifest1, catch_failures: true, debug: false, trace: true)
      apply_manifest(manifest1, catch_changes: true, debug: false, trace: true)
    end

    # Specs are https://serverspec.org/ based
    describe package('zsh') do
      it { is_expected.to be_installed }
    end

    describe group('bridge') do
      it 'creates bridge as expected' do
        is_expected.to exist
        is_expected.to have_gid 1701
      end
    end

    describe group('wriker') do
      it 'creates wriker\'s group' do
        is_expected.to exist
        is_expected.to have_gid 1200
      end
    end

    describe user('wriker') do
      it 'creates wriker as specified' do
        is_expected.to exist
        is_expected.to have_uid 1200
        is_expected.to belong_to_primary_group 'wriker'
        is_expected.to have_login_shell '/bin/zsh'
        is_expected.to have_home_directory '/home/wriker'
        is_expected.to belong_to_group 'wheel'
        is_expected.to belong_to_group 'bridge'
        is_expected.to have_authorized_key rsa_key1
      end
    end

    it 'applies updates without errors and is idempotent on the second run' do
      apply_manifest(manifest2, catch_failures: true, debug: false, trace: true)
      apply_manifest(manifest2, catch_changes: true, debug: false, trace: true)
    end

    describe user('wriker') do
      it 'updates wriker as specified' do
        is_expected.to exist
        is_expected.to have_uid 1200
        is_expected.to belong_to_primary_group 'wriker'
        is_expected.to have_login_shell '/bin/zsh'
        is_expected.to have_home_directory '/home/wriker'
        is_expected.to belong_to_group 'wheel'
        is_expected.to belong_to_group 'bridge'
        is_expected.to have_authorized_key rsa_key2
      end
    end
  end
end
