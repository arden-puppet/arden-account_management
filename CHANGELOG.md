# Changelog

All notable changes to this project will be documented in this file.

## Release 0.3.0

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.2.2...0.3.0#)

**Features**
* Adds explicit support for Raspbian
* Tests sshkey parameters
* Validates sshkey strings
* Disables group creation when a primary group name not matching the user is specified
* Moves to a current release of puppetlabs/accounts

## Release 0.2.2

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.2.1...0.2.2#)

**Features**
* Adds support for Debian in the default setup
* PDK 1.14.1.x config update

## Release 0.2.1

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.2.0...0.2.1#)

**Features**
* Allow alternative modes for exec_map: *creates* or *onlyif*
* Reworked type definition for required groups.

## Release 0.2.0

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.1.3...0.2.0#)

**Features**
* Cron definitions on a per-user basis
* Run-once exec list per user

## Release 0.1.3

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.1.2...0.1.3#)

**Features**
* PDK 1.9.x config update
* Gentoo specific spec tests
* Correct compatibility with ensure_packages from puppet-stdlib

## Release 0.1.2

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.1.1...0.1.2#)

**Features**

* Added support for the Gentoo OS family.

## Release 0.1.1

[Full Changelog](https://gitlab.com/arden-puppet/arden-account_management/compare/0.1.0...0.1.1#)

**Closed issues:**

* corrected shell mapping and added error handling per issue #1

## Release 0.1.0

**Features**

Initial release.
